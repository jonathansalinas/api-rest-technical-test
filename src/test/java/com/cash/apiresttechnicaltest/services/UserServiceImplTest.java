package com.cash.apiresttechnicaltest.services;

import com.cash.apiresttechnicaltest.entities.User;
import com.cash.apiresttechnicaltest.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceImplTest {

    UserRepository userRepository = Mockito.mock(UserRepository.class);

    @Autowired
    UserService userService = new UserServiceImpl(userRepository);

    @BeforeEach
    void setUp() {
        //Mockito
    }

    @Test
    void findByIdWithDataTest() throws Exception {
        Long id = 1l;
        Optional<User> userRepoRespMocked = mockOptionalUser(id);
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(userRepoRespMocked);

        User user = userService.findById(id);

        Assertions.assertNotNull(user);
        Assertions.assertEquals(user.getId(), userRepoRespMocked.get().getId());
        Assertions.assertEquals(user.getFirstName(), userRepoRespMocked.get().getFirstName());
        Assertions.assertEquals(user.getLastName(), userRepoRespMocked.get().getLastName());
        Assertions.assertEquals(user.getEmail(), userRepoRespMocked.get().getEmail());
        Assertions.assertEquals(user.getLoans(), userRepoRespMocked.get().getLoans());
    }

    private Optional<User> mockOptionalUser(Long id) {
        User user = new User();
        user.setId(1l);
        user.setEmail("user1@mail.com");
        user.setFirstName("user");
        user.setFirstName("lastName");
        user.setLoans(Collections.emptySet());
        return Optional.of(user);
    }

    @Test
    void findByIdWithExceptionTest() {
        Mockito.when(userRepository.findById(Mockito.anyLong())).thenReturn(Optional.empty());
        Assertions.assertThrows(Exception.class, () -> {
            userService.findById(Mockito.anyLong());
        });
    }

    @Test
    void findAll() {
        Mockito.when(userRepository.findAll()).thenReturn(mockUserList());

        List<User> userList = userService.findAll();

        Assertions.assertNotNull(userList);
        Assertions.assertEquals(userList.size(), 2);

        Assertions.assertEquals(userList.get(0).getId(), 1l);
        Assertions.assertEquals(userList.get(0).getFirstName(), "user");
        Assertions.assertEquals(userList.get(0).getLastName(), "lastName");
        Assertions.assertEquals(userList.get(0).getEmail(), "user1@mail.com");

        Assertions.assertEquals(userList.get(1).getId(), 2l);
        Assertions.assertEquals(userList.get(1).getFirstName(), "secondUser");
        Assertions.assertEquals(userList.get(1).getLastName(), "lastName");
        Assertions.assertEquals(userList.get(1).getEmail(), "user2@mail.com");
    }

    private List<User> mockUserList() {
        User firstUser = new User();
        firstUser.setId(1l);
        firstUser.setEmail("user1@mail.com");
        firstUser.setFirstName("user");
        firstUser.setLastName("lastName");

        User secondUser = new User();
        secondUser.setId(2l);
        secondUser.setEmail("user2@mail.com");
        secondUser.setFirstName("secondUser");
        secondUser.setLastName("lastName");

        return List.of(firstUser, secondUser);
    }

}