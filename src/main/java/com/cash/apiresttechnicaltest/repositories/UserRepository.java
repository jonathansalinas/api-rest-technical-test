package com.cash.apiresttechnicaltest.repositories;

import com.cash.apiresttechnicaltest.entities.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findAll();
}
