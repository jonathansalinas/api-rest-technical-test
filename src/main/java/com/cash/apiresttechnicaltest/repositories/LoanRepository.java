package com.cash.apiresttechnicaltest.repositories;

import com.cash.apiresttechnicaltest.entities.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface LoanRepository extends PagingAndSortingRepository<Loan, Long> {

    Page<Loan> findByUserId(Long userId, Pageable pageable);
    Page<Loan> findAll(Pageable pageable);

}
