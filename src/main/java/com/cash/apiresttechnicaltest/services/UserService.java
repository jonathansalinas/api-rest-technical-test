package com.cash.apiresttechnicaltest.services;

import com.cash.apiresttechnicaltest.entities.User;

import java.util.List;

public interface UserService {
    User save(User user) throws Exception;
    User findById(Long id) throws Exception;
    List<User> findAll();
    void delete(Long id);
}
