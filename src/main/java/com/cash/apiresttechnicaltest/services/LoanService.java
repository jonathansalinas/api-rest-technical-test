package com.cash.apiresttechnicaltest.services;

import com.cash.apiresttechnicaltest.entities.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface LoanService {

    Page<Loan> findPaged(Long user_id, Pageable pageable);
}
