package com.cash.apiresttechnicaltest.services;

import com.cash.apiresttechnicaltest.entities.Loan;
import com.cash.apiresttechnicaltest.repositories.LoanRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class LoanServiceImpl implements LoanService {

    private final LoanRepository repository;

    public Page<Loan> findPaged(Long user_id, Pageable pageable) {
        if (user_id != null) {
            return repository.findByUserId(user_id, pageable);
        }
        return  repository.findAll(pageable);
    }

}
