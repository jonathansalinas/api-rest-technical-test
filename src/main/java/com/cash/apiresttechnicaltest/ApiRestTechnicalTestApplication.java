package com.cash.apiresttechnicaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestTechnicalTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestTechnicalTestApplication.class, args);
	}

}
