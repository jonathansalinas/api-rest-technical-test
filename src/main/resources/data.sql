INSERT INTO users (id, email, firstName, lastName) VALUES (1, 'marta@mail.com', 'Marta', 'Fernandez');
INSERT INTO users (id, email, firstName, lastName) VALUES (2, 'ramiro@mail.com', 'Ramiro', 'Garcia');
INSERT INTO users (id, email, firstName, lastName) VALUES (3, 'flor@mail.com', 'Florencia', 'Preiz');
INSERT INTO users (id, email, firstName, lastName) VALUES (4, 'agustina@mail.com', 'Agustina', 'Ramirez');
INSERT INTO users (id, email, firstName, lastName) VALUES (5, 'jonathan@mail.com', 'Jonathan', 'Salinas');

INSERT INTO loans (total, user_id)
VALUES (1500.5, 1);